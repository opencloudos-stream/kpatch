%define kpatch_dnf_ver	0.4

Summary:	Live kernel patch manager
Name:		kpatch
Version:	0.9.9
Release:	3%{?dist}
License:	GPLv2
URL:		https://github.com/dynup/kpatch
Source0:	%{url}/archive/refs/tags/v{version}.tar.gz #/kpatch-0.9.9.tar.gz
Source1:	https://github.com/dynup/kpatch-dnf/archive/refs/tags/v{kpatch_dnf_ver}.tar.gz #/kpatch-dnf-0.4.tar.gz
#From rhel
Patch3000:	0002-kpatch-clarify-unload-unsupport.patch
Patch3001:	0003-do-not-rm-selinux-rpm-owned-directory.patch

BuildArch:	noarch
Requires:	bash kmod binutils
Recommends:	kpatch-dnf


%description
kpatch is a Linux dynamic kernel patching infrastructure which allows you to
patch a running kernel without rebooting or restarting any processes.
It enables sysadmins to apply critical security patches to the kernel immediately,
without having to wait for long-running tasks to complete, for users to log off,
or for scheduled reboot windows.
It gives more control over uptime without sacrificing security or stability.


%package -n kpatch-dnf
Summary:	Dnf plugin which installs kpatch-patch RPMs for installed kernels
Version:	%{version}_%{kpatch_dnf_ver}
BuildRequires:	python3-devel python3-dnf
Requires:	python3-dnf python3-hawkey
Provides:	kpatch-dnf

%description -n kpatch-dnf
kpatch-dnf is a DNF plugin that manages subscription to kpatch-patch updates.
When enabled, kernel packages are automatically subscribed to corresponding
kpatch-patch packages updates.


%prep
%autosetup -p1

%setup -D -T -a 1


%build
make -C man
make -C kpatch-dnf-%{kpatch_dnf_ver}


%install
make install PREFIX=/usr DESTDIR=%{buildroot} -C kpatch
make install PREFIX=/usr DESTDIR=%{buildroot} -C man
make install PREFIX=/usr DESTDIR=%{buildroot} -C contrib
mkdir -p %{buildroot}/%{_sharedstatedir}/kpatch
rm -f %{buildroot}/usr/share/man/man1/kpatch-build.1.gz

make install PREFIX=/usr DESTDIR=%{buildroot} PYTHONSITES=%{python3_sitelib} -C kpatch-dnf-%{kpatch_dnf_ver}


%post -n kpatch-dnf
echo "To enable automatic kpatch-patch subscription, run:"
echo -e "\t$ dnf kpatch auto"


%files
%license COPYING
%doc README.md
%{_sbindir}/kpatch
%{_prefix}/lib/systemd/system/kpatch.service
%{_sysconfdir}/init/kpatch.conf
%{_sharedstatedir}/kpatch
%doc %{_mandir}/man1/kpatch.1.gz


%files -n kpatch-dnf
%{python3_sitelib}/dnf-plugins/kpatch.py
%{python3_sitelib}/dnf-plugins/__pycache__
%config(noreplace) %{_sysconfdir}/dnf/plugins/kpatch.conf
%doc %{_mandir}/man8/dnf.kpatch.8.gz


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.9-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.9-2
- Rebuilt for loongarch release

* Wed Mar 27 2024 anatasluo <anatasluo@tencent.com> - 0.9.9-1
- Upgrade to v0.9.9

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.7-5
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.7-4
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.7-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.9.7-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Nov 28 2022 rockerzhu <rockerzhu@tencent.com> - 0.9.7-1
- Initial build
